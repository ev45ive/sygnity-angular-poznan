https://bitbucket.org/ev45ive/sygnity-angular-poznan/

https://bitbucket.org/ev45ive/sygnity-angular-poznan/downloads/


src/master/src/app/security/
src/master/src/app/app.module.ts
src/master/src/environments/environment.ts

# GIT
cd ..
git clone https://bitbucket.org/ev45ive/sygnity-angular-poznan/

# Versions
node -v
code -v 
npm -v
git --version

# Nowy projekt
npm i -g @angular/cli
ng version


cd ..

ng new sygnity-angular 
Would you like to add Angular routing? (y/N) Y
Which stylesheet format would you like to use? (Use arrow keys) SCSS

# Package.json + Semver

https://semver.org/

https://nodesource.com/blog/semver-tilde-and-caret/


# Run development server
ng serve --port 8080
<!-- Lub -->
ng s -o 
 
# Angular Language service
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template


# Generatory

<!-- src/app/playlists/playlists.module.ts -->
ng g m playlists -m app --routing true 

ng g c playlists/views/playlists-view --export 

ng g c playlists/components/items-list
ng g c playlists/components/list-item

ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

# Bootstrap
npm i bootstrap --save


# Directives

ng g m shared -m playlists
<!-- // app/shared/shared.module.ts -->

ng g d shared/highlight --export 

# Card 

ng g c shared/card --export


# Music Search

ng g m music-search -m app --routing

ng g c music-search/views/music-search --export

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

<!-- music-search.component.html -->
.row>.col>app-search-form^^.row>.col>app-search-results

<!-- Search form - components/search-form.component.html -->
https://getbootstrap.com/docs/4.4/components/input-group/#button-addons

<!-- Results  - components/search-results.component.html -->
https://getbootstrap.com/docs/4.4/components/card/#card-groups

<!-- Album card  - components/album-card.component.html -->
https://getbootstrap.com/docs/4.4/components/card/#images

https://www.placecage.com/300/300
https://www.place-hoff.com/
https://www.placekitten.com/
https://www.placeimg.com/

# Services

ng g s music-search/services/music-search


ng g m security -m app

ng g s security/auth 

https://github.com/manfredsteyer/angular-oauth2-oidc


# Http Interceptor

ng g s security/auth-interceptor 


# BUild

ng build 
ng build --prod

cd dist/sygnity-angular
kopiujemy na serwer produkcyjny