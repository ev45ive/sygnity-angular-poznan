import { Injectable } from "@angular/core";
import { HttpParams, HttpClient, HttpBackend } from "@angular/common/http";
import { tap } from "rxjs/operators";

// https://github.com/manfredsteyer/angular-oauth2-oidc

export abstract class AuthConfig {
  auth_url!: string;
  token_url!: string;
  client_id!: string;
  redirect_uri!: string;
  response_type: "code" | "token" = "code";
  state?: string;
  /**
   * https://developer.spotify.com/documentation/general/guides/scopes/
   *
   * */
  scope?: string;
  show_dialog?: string;
}

export interface TokenObject {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
  scope: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private http = new HttpClient(this.httpBackend);

  constructor(private httpBackend: HttpBackend, private config: AuthConfig) {
    this.extractToken();
  }

  authorize() {
    const {
      auth_url,
      client_id,
      redirect_uri,
      response_type,
      show_dialog
    } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type,
        show_dialog: show_dialog!
      }
    });

    location.replace(`${auth_url}?${p.toString()}`);
  }

  private extractToken() {
    let tokenRaw = sessionStorage.getItem("token");
    if (tokenRaw) {
      this.token = JSON.parse(tokenRaw);
      return;
    }

    const p = new HttpParams({
      fromString: window.location.search
    });
    const code = p.get("?code");
    if (code) {
      this.requestToken(code);
    } else {
      this.authorize();
    }
  }

  private token: TokenObject | null = null;

  getToken() {
    return this.token?.access_token;
  }

  refreshToken() {
    if (!this.token) {
      this.authorize();
    }

    return this.http
      .post<TokenObject>(
        this.config.token_url,
        new HttpParams({
          fromObject: {
            refresh_token: this.token!.refresh_token,
            grant_type: "refresh_token",
            redirect_uri: this.config.redirect_uri
          }
        }).toString(),
        {
          // reportProgress:true
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            // Base64(client_id:client_secret)
            Authorization:
              "Basic NzA1OTllZTU4MTJhNGExNmFiZDg2MTYyNWEzOGY1YTY6NjI0ZDY4ZWQ3ZTNkNGRiNjg1OTU3NDgxYmY4YTJiMWM="
          }
        }
      )
      .pipe(
        tap((data: TokenObject) => {
          this.token!.access_token = data.access_token;
          sessionStorage.setItem("token", JSON.stringify(this.token));
        })
      );
  }

  requestToken(code: string) {
    const req = this.http.post<TokenObject>(
      this.config.token_url,
      new HttpParams({
        fromObject: {
          code,
          grant_type: "authorization_code",
          redirect_uri: this.config.redirect_uri
        }
      }).toString(),
      {
        // reportProgress:true
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          // Base64(client_id:client_secret)
          Authorization:
            "Basic NzA1OTllZTU4MTJhNGExNmFiZDg2MTYyNWEzOGY1YTY6NjI0ZDY4ZWQ3ZTNkNGRiNjg1OTU3NDgxYmY4YTJiMWM="
        }
      }
    );

    req.subscribe({
      next: (data: TokenObject) => {
        this.token = data;
        sessionStorage.setItem("token", JSON.stringify(this.token));
      },
      error: error => {
        console.error(error);
      },
      complete: () => {
        console.log("complete");
      }
    });
  }
}
