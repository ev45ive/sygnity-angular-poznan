import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { AuthService } from "./auth.service";
import { catchError, switchMap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const authReq = this.addToken(req);

    return next.handle(authReq).pipe(
      catchError((err, caught) => {
        if (!(err instanceof HttpErrorResponse)) {
          return throwError(err);
        }

        if (err.status === 401) {
          return this.auth
            .refreshToken()
            .pipe(switchMap(() => next.handle(this.addToken(req))));
        }

        return throwError(new Error(err.error.error.message));
      })
    );
  }

  private addToken(req: HttpRequest<any>) {
    return req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      }
    });
  }
}
