import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/models/playlist";
import { ActivatedRoute, Router } from "@angular/router";
import { map, filter } from "rxjs/operators";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent {
  mode = "show"; // 'edit'

  selected: Playlist | null = null;

  playlists: Playlist[] = [
    {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular Top20",
      favorite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: false,
      color: "#00ffff"
    }
  ];

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        map(p => p.get("playlist_id")),
        filter(id => id != null),
        map(id => this.playlists.find(p => p.id == parseInt(id!)))
      )
      .subscribe(selected => {
        this.selected = selected!;
      });
  }

  select(p: Playlist) {
    this.router.navigate(["/playlists", p.id], {
      // replaceUrl: true
    });
  }

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  // (save)="save($event)" :

  save(draft: Playlist) {
    this.mode = "show";
    this.selected = draft;

    this.playlists = this.playlists.map(p => (p.id === draft.id ? draft : p));
  }
}
