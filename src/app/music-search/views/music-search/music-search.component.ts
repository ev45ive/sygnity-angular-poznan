import { Component, OnInit } from "@angular/core";
import { MusicSearchService } from "../../services/music-search.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
})
export class MusicSearchComponent implements OnInit {
  resultsChange = this.service.getAlbums();
  query = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {}

  ngOnInit() {
    // this.query = this.route.snapshot.queryParamMap.get("query")!;

    this.route.queryParamMap.subscribe(queryParamMap => {
      this.query = queryParamMap.get("query")!;

      if (this.query) {
        this.service.search(this.query);
      }
    });
  }

  search(q: string) {
    this.router.navigate(
      [
        /* "/search" */
      ],
      {
        queryParams: {
          query: q
        },
        replaceUrl: true,
        relativeTo: this.route
      }
    );
  }
}
