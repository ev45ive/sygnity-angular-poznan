import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  map,
  withLatestFrom
} from "rxjs/operators";
import { Observable, combineLatest } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    return ("" + control.value).includes("batman")
      ? {
          censor: { badword: "batman" }
        }
      : null;
  };

  asyncCensor: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // return this.http.post(validator_url, control.value).pipe(map(resp => /*.error.*/ | null )

    return new Observable<ValidationErrors | null>(subscriber => {
      const timeoutHandler = setTimeout(() => {
        subscriber.next(this.censor(control));
        subscriber.complete();
      }, 1000);

      // OnUnsubscribe / onComplete:
      return () => {
        clearTimeout(timeoutHandler);
      };
    });
  };

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor
      ],
      [this.asyncCensor]
    ),
    type: new FormControl("album")
  });

  constructor() {
    // console.log(this.queryForm.clearValidators());
    (window as any).form = this.queryForm;

    console.log(this.queryForm);

    const validChanges: Observable<string> = this.queryForm.controls.query.statusChanges.pipe(
      filter(status => status === "VALID")
    );

    const valueChanges: Observable<string> = this.queryForm.controls.query.valueChanges.pipe(
      // nie za czesto
      debounceTime(400),

      // min 3 znaki
      filter(query => query.length >= 3),

      // bez duplikatow
      distinctUntilChanged()
    );

    const searchChanges = validChanges.pipe(
      withLatestFrom(valueChanges),
      map(([status, value]) => value)
    );

    // combineLatest(valueChanges, validChanges)
    //   .pipe(map(([value, status]) => value))

    searchChanges.subscribe(q => this.search(q));
    // .subscribe(console.log);
  }

  ngOnInit(): void {}

  @Output() queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }
}
