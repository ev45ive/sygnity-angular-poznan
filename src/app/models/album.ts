// ng g i models/album

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists: Artist[];
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}

