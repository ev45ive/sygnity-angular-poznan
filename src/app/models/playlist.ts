interface Entity {
  id: number;
  name: string;
}

export interface Track extends Entity {}

export interface Playlist extends Entity {
  favorite: boolean;
  /**
   * HEX Color
   */
  color: string;
  tracks?: Track[];
}

// if(p.tracks){
//   p.tracks.length
// }

// interface Playlist {
//   id: number | string;
// }

// if('string' == typeof p.id){
//   p.id.trim()
// }else{
//   p.id.toExponential()
// }

// interface Vector {
//   x: number;
//   y: number;
// }

// interface Point {
//   x: number;
//   y: number;
// }

// const q: Vector = { x: 1, y: 2 };
// const s: Point = q

// class Playlist {
//   constructor(
//     private id: number,
//     protected name: string,
//     public favourite: boolean
//   ) {}
// }
// new Playlist(123, "123", true);
