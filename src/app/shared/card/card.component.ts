import {
  Component,
  OnInit,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { NgForm, NgModel } from "@angular/forms";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"]
})
export class CardComponent implements OnInit {
  
  // @ContentChildren(NgModel, { read: NgModel, descendants: true })
  // childFormRef?: QueryList<NgModel>;

  // ngAfterContentChecked() {
  //   console.log(this.childFormRef);
  // }

  constructor() {}

  ngOnInit(): void {}
}
