import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";
import { MusicSearchModule } from "./music-search/music-search.module";
import { SecurityModule } from "./security/security.module";
import { environment } from "../environments/environment";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlaylistsModule,
    SharedModule,
    MusicSearchModule,
    SecurityModule.forRoot(environment.authConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
